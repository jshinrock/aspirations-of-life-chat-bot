const mongoose = require('mongoose');

const requestSchema = new mongoose.Schema({
  phone: String,
  message: String,
}, { timestamps: true });

const Request = mongoose.model('Request', requestSchema);

module.exports = Request;
