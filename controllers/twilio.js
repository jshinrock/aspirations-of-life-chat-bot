'use strict';
const _ = require('lodash');
const async = require('async');
const validator = require('validator');
const request = require('request');
const twilio = require('twilio')(process.env.TWILIO_SID, process.env.TWILIO_TOKEN);
const Request = require('../models/Request');

/**
 * GET /api/twilio
 * Twilio API example.
 */
exports.getTwilio = (req, res) => {
  res.render('api/twilio', {
    title: 'Twilio API'
  });
};

exports.receiveSms = (req, res) => {
  console.log('TWILIO - receive sms from [ %s ]', req.body.From);
  console.log(req.body);
  
  let url;
  
  try{
      url = process.env.TWILIO_URL;
  } catch(e) {
      console.error('No TWILIO_URL specified in environment variables');
      url = 'http://aspirationsoflife.org/';
  }

  const message = {
    to: req.body.From,
    from: process.env.TWILIO_NUMBER,
    body: url
  };
  
  twilio.sendMessage(message, (err, responseData) => {
    if (err) { 
        console.log('TWILIO - error sending message');
        res.send(500, err);
    }
    
    if(responseData && responseData.to){
        console.log('TWILLIO - MESSAGE SENT TO [ %s ]', responseData.to);
    }
    // save number for lookup later
    const user = new Request({
        phone: req.body.From,
        message: req.body.Body
    });
    
    user.save((err) => {
      if (err) { 
          console.log("MONGOOSE ERROR");
          console.log(err);
          res.send(500, err); 
      }
      console.log('User information stored');
    });
  });
    
}

/**
 * POST /api/twilio
 * Send a text message using Twilio.
 */
exports.postTwilio = (req, res, next) => {
  req.assert('number', 'Phone number is required.').notEmpty();
  req.assert('message', 'Message cannot be blank.').notEmpty();

  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    //return res.redirect('/api/twilio');
    console.log('TWILLIO POST ERROR');
    console.log(errors);
  }

  const message = {
    to: req.body.number,
    from: '+15152079669',
    body: req.body.message
  };
  twilio.sendMessage(message, (err, responseData) => {
    if (err) { return next(err.message); }
    console.log('TWILLIO - MESSAGE SENT TO [ ${responseData.to} ]');
    //req.flash('success', { msg: `Text sent to ${responseData.to}.` });
    //res.redirect('/api/twilio');
  });
};
