const Request = require('../models/Request');
const _ = require('underscore');
const Utils = require('../helpers/util');

/**
 * GET /
 * Home page.
 */
exports.index = (req, res) => {

  Request.find( (err, users) => {})
  .then( (users) => {
      
      console.log(users);
            
      var sortedUsers = _.groupBy(users, 'phone');
            
      res.render('users', {
          title: 'Users',
          users: users
      });
  
  });
  
};
